$(document).ready(function () {
  $("#min_price1,#max_price1").on("paste keyup", function () {
    var min_price_range = parseInt($("#min_price1").val());

    var max_price_range = parseInt($("#max_price1").val());

    if (min_price_range == max_price_range) {
      max_price_range = min_price_range + 100;

      $("#min_price1").val(min_price_range);
      $("#max_price1").val(max_price_range);
    }

    $("#slider-range1").slider({
      values: [min_price_range, max_price_range],
    });
  });

  $(function () {
    $("#slider-range1").slider({
      range: true,
      orientation: "horizontal",
      min: 0,
      max: 1000000,
      values: [1000, 8000],
      step: 1,
      margin: "82px",
      slide: function (event, ui) {
        if (ui.values[0] == ui.values[1]) {
          return false;
        }

        $("#min_price1").val(ui.values[0]);
        $(".from").html(ui.values[0]);
        $("#max_price1").val(ui.values[1]);
        $(".to").html(ui.values[1]);
      },
    });

    $("#min_price1").val($("#slider-range1").slider("values", 0));
    $("#max_price1").val($("#slider-range1").slider("values", 1));
  });

  $("#min_price2,#max_price2").on("paste keyup", function () {
    var min_price_range = parseInt($("#min_price2").val());

    var max_price_range = parseInt($("#max_price2").val());

    if (min_price_range == max_price_range) {
      max_price_range = min_price_range + 100;

      $("#min_price2").val(min_price_range);
      $("#max_price2").val(max_price_range);
    }

    $("#slider-range2").slider({
      values: [min_price_range, max_price_range],
    });
  });

  $(function () {
    $("#slider-range2").slider({
      range: true,
      orientation: "horizontal",
      min: 0,
      max: 10000,
      values: [1000, 8000],
      step: 1,

      slide: function (event, ui) {
        if (ui.values[0] == ui.values[1]) {
          return false;
        }

        $("#min_price2").val(ui.values[0]);
        $("#max_price2").val(ui.values[1]);
      },
    });

    $("#min_price2").val($("#slider-range2").slider("values", 0));
    $("#max_price2").val($("#slider-range2").slider("values", 1));
  });
});
