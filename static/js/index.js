var headerHam = document.querySelector(".header-catalog-ham");
var allWrapper = document.querySelector(".all-wrapper");
var footerFirstContainer = document.querySelector(".footer-first-container");
var leftSidebar = document.querySelector(".header__container .left-sidebar");
var catItem = document.querySelectorAll(".catalogue-menu > ul > li");
let feedbackDrops = document.querySelectorAll(".main-comp__feedback-drop");

$(".katalog1").on("click", function () {
  $(this).toggleClass("active").next().slideToggle(500);
});
$(".katalog2").on("click", function () {
  $(this).toggleClass("active").next().slideToggle(500);
  $(".katalog2").not($(this)).removeClass("active");
  $(".katalog2").next().not($(this).next()).slideUp(500);
});
$(".katalog3").on("click", function () {
  $(this).toggleClass("active").next().slideToggle(500);
  $(".katalog3").not($(this)).removeClass("active");
  $(".katalog3").next().not($(this).next()).slideUp(500);
});

$(".multimeters-sidebar-open-btn").on("click", function () {
  if ($(".multimeters-sidebar-open").hasClass("active")) {
    $(".multimeters-sidebar-open").slideUp().removeClass("active");
    $(this).html(" Фильтр ");
  } else {
    $(".multimeters-sidebar-open").slideDown().addClass("active");
    $(this).html("закрыть");
  }
});

let catalogChildItems = [
  ...document.querySelectorAll(".catalogue-menu__child-items"),
];

if ($(".main-slider").length) {
  $(".main-slider").slick({
    dots: true,
  });
}

$(".header__side-toggle").on("click", function () {
  $("body").addClass("active");
  $(".main-sidebar").addClass("active");
  $(".sidebar-wrapper").addClass("active");
  $(".main-sidebar__close").addClass("active");
});
$(".main-sidebar__close").on("click", function () {
  $(".main-sidebar").removeClass("active");
  $(".sidebar-wrapper").removeClass("active");
  $("body").removeClass("active");
  $(this).removeClass("active");
  $(".katalog2").next().slideUp();
  $(".katalog1").next().slideUp();
  $(".katalog3").next().slideUp();
});
$(document).on("click", function (e) {
  if ($(e.target).hasClass("sidebar-wrapper")) {
    $("body").removeClass("active");
    $(".main-sidebar").removeClass("active");
    $(".sidebar-wrapper").removeClass("active");
    $(".main-sidebar__close").removeClass("active");
  }
  //$(".sidebar-wrapper").removeClass("active");
});

let thumbnails = [
  ...document.querySelectorAll(".prodetails__box-thumbnails > ul > li"),
];
let bigBox = document.querySelector(".prodetails__box-big img");

if ($(".xzoom").length) {
  $(".xzoom").xzoom();
}
$(thumbnails).click(function () {
  var curImg = $(this).attr("data-img");
  bigBox.src = curImg;
  $(bigBox).attr("xoriginal", curImg);
  $(this).addClass("active");
  $(this).siblings().removeClass("active");
});
$(".formblock__btn").click(function () {
  console.log($(".star-rating-input").val());
});

$(".formblock__btn").on("click", function () {
  $(".star-rating-input").value = $(".form-rate").attr("rate");
});
const formstars = document.querySelectorAll(".icon svg");

for (let i = 0; i < formstars.length; i++) {
  formstars[i].addEventListener("mouseover", function () {
    for (let j = 0; j < formstars.length; j++) {
      formstars[j].style.color = "#f9b90b";
      formstars[j].style.color = "#cddaeb";
    }
    for (let j = 0; j <= i; j++) {
      formstars[j].style.color = "#cddaeb";
      formstars[j].style.color = "#f9b90b";
    }
  });
}
$(".range-item__title").each(function () {
  this.addEventListener("click", function () {
    $(this).siblings().slideToggle();
    $(this).children().last().toggleClass("rotate");
  });
});
headerHam.addEventListener("click", function (e) {
  this.classList.toggle("active");

  if (headerHam.classList.contains("active")) {
    if ($(document).width() > "1284") {
      this.style.width = "177px";
    }
    $(".header__catalog p").css("color", "#fff");
    allWrapper.classList.add("black-bg");
    leftSidebar.classList.add("active");
  } else {
    {
      allWrapper.classList.remove("black-bg");
      $(".header__catalog p").css("color", "#7b93b0");
      this.style.width = "36px";
      leftSidebar.classList.remove("active");
    }
  }
});

$(".rating-stars-form > div").mouseenter(function () {
  $(this).addClass("hovered");

  $(this).mouseleave(function () {
    $(this).removeClass("hovered");
  });
});

$(".rating-stars-form > div").click(function () {
  var starIndex = parseInt($(this).index()) + 1;
  $(this).parent().attr("rate", starIndex);
});
$(".rating-stars-form").mouseenter(function () {
  $(this).addClass("hovered");
  $(this).mouseleave(function () {
    $(this).removeClass("hovered");
  });
});

$(".main-slider-block__sidebar .left-sidebar").mouseenter(function () {
  var _self = $(this);
  _self.addClass("entered");
  setTimeout(function () {
    if (_self.hasClass("entered")) {
      headerHam.style.width = "177px";
      $(".header__catalog p").css("color", "#fff");
      headerHam.classList.add("active");
      allWrapper.classList.add("black-bg");
      leftSidebar.classList.add("active");
      $("header").addClass("hovered");
    }
  }, 300);
});

$(".main-slider-block__sidebar .left-sidebar").mouseleave(function () {
  $(this).removeClass("entered");
});

$(".home-header-wrapper .left-sidebar").mouseleave(function () {
  if ($("header").hasClass("hovered")) {
    closeCatsOnHover();
  }
});

function closeCatsOnHover() {
  $(".main-slider-block__sidebar .left-sidebar").removeClass("entered");
  allWrapper.classList.remove("black-bg");
  headerHam.classList.remove("active");
  $(".header__catalog p").css("color", "#7b93b0");
  headerHam.style.width = "36px";
  leftSidebar.classList.remove("active");
  $("header").removeClass("hovered");
}

window.addEventListener("click", function (e) {
  if (
    e.target.classList.contains("black-bg") ||
    e.target.classList.contains("header")
  ) {
    allWrapper.classList.remove("black-bg");
    headerHam.classList.remove("active");
    $(".header__catalog p").css("color", "#7b93b0");
    headerHam.style.width = "36px";
    leftSidebar.classList.remove("active");
  }
});
