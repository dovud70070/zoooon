from django.db import models
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey
from filer.fields import image as filer_image
from django.db.models import Manager


class Category(MPTTModel):
    title = models.CharField(max_length=200, blank=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    slug = models.SlugField(max_length=200, blank=True, null=True)
    icon = models.ImageField(upload_to='media/', blank=True, null=True)
    image = models.ImageField(upload_to='media/', blank=True, null=True)
    objects = Manager()

    class MPTTMeta:
        ordering_insertion_by = ['title']

    def get_children(self):
        return Category.objects.filter(parent=self)

    def get_absolute_url(self):
        return reverse('product_list', args=[self.slug])

    @property
    def is_parent(self):
        if self.parent is not None:
            return False
        return True

    def __str__(self):
        return self.title


class Product(models.Model):
    STATUS = (
        ("new", 'New'),
        ('top', 'Top')
    )
    name = models.CharField(max_length=200, blank=True, null=True)
    main_image = filer_image.FilerImageField(on_delete=models.CASCADE)
    category = TreeForeignKey(Category, on_delete=models.CASCADE, related_name='related_products')
    old_price = models.FloatField()
    status = models.CharField(max_length=200, choices=STATUS, blank=True, null=True)
    quantity = models.IntegerField(default=1)
    available = models.BooleanField(default=False)
    slug = models.SlugField(max_length=200, blank=True)
    current_price = models.FloatField()

    def __str__(self):
        return self.name


class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images')
    image = filer_image.FilerImageField(on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.product.name
