from django.contrib import admin
from .models import Category, Product, ProductImage
from mptt.admin import MPTTModelAdmin, DraggableMPTTAdmin


class CategoryAdmin(DraggableMPTTAdmin):
    list_display = ('tree_actions', 'indented_title')
    list_display_links = ('indented_title',)
    search_fields = ('title',)
    prepopulated_fields = {'slug': ('title',)}


class ProductImageInline(admin.StackedInline):
    model = ProductImage
    fields = ['image']


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'old_price', 'status', 'available')
    prepopulated_fields = {'slug': ('name',)}
    inlines = [ProductImageInline, ]


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
