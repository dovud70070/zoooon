from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView, ListView, DetailView
from .models import Category, Product
from django.db.models import Sum, Value, IntegerField


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['home_category'] = Category.objects.all()[:6]
        context['products'] = Product.objects.filter(available=True)[:6]
        print(context)
        return context


class ProductListView(ListView):
    template_name = 'measureTech.html'
    model = Product
    context_object_name = 'products'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        category_slug = self.kwargs.get('category_slug', None)
        if category_slug:
            category = get_object_or_404(Category, slug=category_slug)
            context['products'] = Product.objects.filter(category=category, available=True)
        return context


class ProductDetailView(DetailView):
    template_name = 'productsDetails.html'
    model = Product
    context_object_name = 'products'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.filter(parent__isnull=True)
        context['products'] = Product.objects.get(slug=self.kwargs.get('slug'))
        print(context)
        return context
